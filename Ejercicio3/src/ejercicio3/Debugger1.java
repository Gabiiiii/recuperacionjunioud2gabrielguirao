package ejercicio3;

public class Debugger1 {
	static final String MENSAJE_FIN = "Fin del programa";

	public static void main(String[] args) {
		int contador = 100;
		
		System.out.println(contador);
		while (contador>0) {
			//El contador debe disminuir cada vez que
			//el bucle se repite en vez de aumentar
			contador--;
			System.out.println(contador);
		}
		System.out.println(MENSAJE_FIN);
	}
	

}
